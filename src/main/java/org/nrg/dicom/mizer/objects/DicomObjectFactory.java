package org.nrg.dicom.mizer.objects;

import org.apache.commons.lang3.StringUtils;
import org.dcm4che2.data.*;
import org.dcm4che2.io.DicomInputStream;
import org.dcm4che2.io.DicomOutputStream;
import org.nrg.dicom.mizer.exceptions.MizerException;
import org.nrg.dicom.mizer.tags.*;
import org.nrg.dicom.mizer.tags.Tag;
import org.nrg.dicom.mizer.values.Value;
import org.nrg.dicom.mizer.visitors.AssignIfExistsDicomObjectVisitor;
import org.nrg.dicom.mizer.visitors.DeleteDicomObjectVisitor;
import org.nrg.dicom.mizer.visitors.DumpDicomTagVisitor;
import org.nrg.dicom.mizer.visitors.OrphanPvtCreatorIDExterminator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.time.Period;
import java.util.*;
import java.util.stream.Collectors;
import java.util.zip.GZIPInputStream;

import static java.util.Arrays.stream;
import static org.dcm4che2.util.TagUtils.isPrivateDataElement;

/**
 * Instantiate concrete implementations of DicomObjectI.
 */
public class DicomObjectFactory {
    public static DicomObjectI newInstance() {
        return new MizerDicomObject();
    }

    public static DicomObjectI newInstance(final File file) throws MizerException {
        return new MizerDicomObject(file);
    }

    public static DicomObjectI newInstance(final InputStream input) throws MizerException {
        return new MizerDicomObject(input);
    }

    /**
     * Create DicomObjectI from the provided dcm4che2 dicom object.
     *
     * This causes the dcm4ch4 lib to leak, but hey, backwards compatability.
     * This is for backwards compatibility with anonymize package.
     *
     * @param matchFile   TODO:  What is this? Ignored for now.
     * @param dicomObject The DICOM object to be processed.
     */
    public static DicomObjectI newInstance(final File matchFile, final DicomObject dicomObject) {
        return new MizerDicomObject(dicomObject);
    }

    public static DicomObjectI newInstance(final DicomObject dicomObject) {
        return new MizerDicomObject(dicomObject);
    }

    private static class MizerDicomObject implements DicomObjectI {

        private DicomObject dobj;

        public MizerDicomObject() {
            this.dobj = new BasicDicomObject();
        }

        public MizerDicomObject(File f) throws MizerException {
            try (final InputStream fin = getInputStream(f); final DicomInputStream dis = new DicomInputStream(fin)) {
                dobj = dis.readDicomObject();
            } catch (IOException e) {
                throw new MizerException(e);
            }
        }

        public MizerDicomObject(final InputStream input) throws MizerException {
            try (final DicomInputStream dis = new DicomInputStream(input)) {
                dobj = dis.readDicomObject();
            } catch (IOException e) {
                throw new MizerException(e);
            }
        }

        public MizerDicomObject(final DicomObject dicomObject) {
            this.dobj = dicomObject;
        }

        /**
         * Return the value(s) at the specified tagPath as a String.
         *
         * The values of tags with multiplicity &gt; 1 are concatenated and separated with the DICOM value-separation character \.
         *
         * @param tagPath The {@link TagPath tag path} to retrieve.
         *
         * @return String representation of value(s) at specified tagPath. null if the tag does not exist, empty string if tag exists but is empty.
         *
         * @throws NumberFormatException if tagPath contains wildcard chars and does not resolve to a unique tag.
         */
        @Override
        public String getString(TagPath tagPath) {
            int[] tagArray = this.resolve(tagPath, false);
            return getString(tagArray);
        }

        public String[] getStrings(TagPath tagPath) {
            int[] tagArray = this.resolve(tagPath, false);
            return getStrings(tagArray);
        }

        /**
         * Return the value(s) at the specified tag as a String.
         *
         * The values of tags with multiplicity &gt; 1 are concatenated and separated with the DICOM value-separation character \.
         *
         * @param tag The tag to retrieve.
         *
         * @return String representation of value(s) at specified tag. null if the tag does not exist, empty string if tag exists but is empty.
         */
        @Override
        public String getString(int tag) {
            int[] tagArray = new int[]{tag};
            final String value =  getString(tagArray);
            if (logger.isDebugEnabled()) {
                logger.debug("Got: {} = {}", TagPath.toString(tagArray), value);
            }
            return value;
        }

        @Override
        public String[] getStrings(int tag) {
            int[] tagArray = new int[]{tag};
            final String[] value =  getStrings(tagArray);
            if (logger.isDebugEnabled()) {
                logger.debug("Got: {} = {}", TagPath.toString(tagArray), value);
            }
            return value;
        }

        /**
         * Return the value(s) at the specified tag array as a String.
         *
         * The values of tags with multiplicity &gt; 1 are concatenated and separated with the DICOM value-separation character \.
         *
         * @param tagArray An array of tags to evaluate.
         *
         * @return String representation of value(s) at specified tag. null if the tag does not exist, empty string if tag exists but is empty.
         */
        @Override
        public String getString(final int... tagArray) {
            String[] strings = getStrings(tagArray);
            return joinMultipleValues( strings);
        }

        protected String joinMultipleValues( String[] strings) {
            return (strings != null) ? stream(strings).collect(Collectors.joining("\\")) : null;
        }

        @Override
        public String[] getStrings(final int... tagArray) {
            // tags with VR == UN do not have an implementation of getStrings(). Are there other VRs that do this?
            // Is there a better way to detect tags with multiplicity > 1?
            if (logger.isTraceEnabled()) {
                logger.trace("Fetching: {}", TagPath.toString(tagArray));
            }
            if( isVR_UnAndPresentAndEmpty(tagArray)) {
                return new String[]{};
            }
            String[] strings;
            try {
                strings = dobj.getStrings(tagArray);
            } catch (UnsupportedOperationException e) {
                strings = new String[1];
                strings[0] = dobj.getString(tagArray);
            }
             return strings;
        }

        @Override
        public byte[] getBytes( int tag) {
            return dobj.getBytes( tag);
        }

        @Override
        public void putBytes( int tag, String vrString, byte[] b) {
            VR vr = VR.UN;
            switch( vrString) {
                case "OB":
                    vr = VR.OB;
                    break;
                case "OW":
                    vr = vr.OW;
                    break;
                default:
                    logger.debug("setting bytes with VR UN.");
                    break;
            }
            dobj.putBytes( tag, vr, b);
        }

        @Override
        public String getVR( int tag) {
            return vr(tag).toString();
        }

        @Override
        public String getVR(TagPath tp) {
            return vr(tp.getTagsAsArray()).toString();
        }

        private VR vr(int tag) {
            // get existing VR if present
            VR vr = (dobj.get(tag) != null)? dobj.get(tag).vr(): null;
            if (vr == null) {
                // look up known value. UN if not found.
                vr = (dobj.vrOf(tag) != null)? dobj.vrOf(tag): VR.UN;
            }
            return vr;
        }

        private VR vr(int tag[]) {
            // get existing VR if present
            VR vr = (dobj.get(tag) != null)? dobj.get(tag).vr(): null;
            if (vr == null) {
                // look up known value. UN if not found.
                vr = (dobj.vrOf(tag[tag.length-1]) != null)? dobj.vrOf(tag[tag.length-1]): VR.UN;
            }
            return vr;
        }

        private boolean isVR_UnAndPresentAndEmpty(int[] tags) {
            return "UN".equals(this.vr(tags).toString())
                    && this.contains(tags)
                    && this.isEmpty(tags);
        }

        @Override
        public Optional<Period> getAge(int tag) {
            String ageString = dobj.getString(tag);
            if (StringUtils.isEmpty( ageString)) {
                return Optional.ofNullable(null);
            }
            try {
                int length = Integer.parseInt(ageString.substring(0,3));
                String units = ageString.substring(3,4);
                switch (units) {
                    case "Y":
                        return Optional.of(Period.ofYears(length));
                    case "M":
                        return Optional.of(Period.ofMonths(length));
                    case "W":
                        return Optional.of(Period.ofWeeks(length));
                    case "D":
                        return Optional.of(Period.ofDays(length));
                    default:
                        throw new IllegalArgumentException(String.format("Unknown age format in tag %d: '%s'", tag, ageString));
                }
            } catch (Exception e) {
                throw new IllegalArgumentException(String.format("Error parsing age in tag %d: '%s'", tag, ageString));
            }
        }

        /**
         * Return the resolved private tag in the context of this dicom object, optionally create the tag if it does not exist.
         *
         * Will create the associated private creator ID if the tag being created requires it.
         *
         * @param tag        tag to be resolved, in range of 0xggggnnee where nn is ignored.
         * @param pvtCreator value of private creator ID of the owner of this tag.
         * @param create     create the resolved tag with empty value if the tag does not exist and this flag is true.
         *
         * @return the resolved private tag with value 0xggggbbee where bb is the block value reserved by the private creator.
         * or -1 if there is no reserved block.
         */
        @Override
        public int resolvePrivateTag(int tag, String pvtCreator, boolean create) {
            return dobj.resolveTag(tag, pvtCreator, create);
        }

        @Override
        public void assign(TagPath tagPath, String value) {
            if (tagPath.isSingular()) {
                if (value == null) {
                    logger.debug("Failed to assign null value to tag: {}", tagPath);
                    return;
                }
                int[] ia = this.resolve(tagPath, true);
                putString(ia, value);
            } else {
                throw new UnsupportedOperationException("Assigning to a tagpath that maps to multiple attributes in not supported. TagPath: " + tagPath);
            }
        }

        @Override
        public void assignIfExists(TagPath tagPath, Value value) {
            DicomObjectVisitor visitor = new AssignIfExistsDicomObjectVisitor(tagPath, value);
            visitor.visit(this);
        }

        @Override
        public void assign(int tag, Value value) {
            putString(tag, value.asString());
        }

        @Override
        public void assign(int[] tagPath, Value value) {
            putString( tagPath, value.asString());
        }

        @Override
        public void delete(int tag) {
            dobj.remove(tag);
        }

        @Override
        public void delete(int[] tags) {
            dobj.remove(tags);
        }

        @Override
        public void deleteAllTags() {
            dobj.clear();
        }

        private DeleteDicomObjectVisitor deleteVisitor = new DeleteDicomObjectVisitor();

        /**
         * delete tags matching the tagpath.
         *
         * One would think one could use tagPathToDelete.isSingular() to avoid visiting all tags, but a singular
         * TagPath may not be singular in the context of some bad-data. In particular, private data with two blocks with
         * the same creator ID in a single group.
         *
         * @param tagPathToDelete
         */
        @Override
        public void delete(final TagPath tagPathToDelete) {
            deleteVisitor.setTagPathToDelete( tagPathToDelete);
            deleteVisitor.visit(this);
        }

        @Override
        public void deleteEmptyPrivateBlocks() {
            DicomObjectVisitor visitor = new OrphanPvtCreatorIDExterminator();
            visitor.visit( this);
        }

        @Override
        public DicomObject getDcm4che2Object() {
            return dobj;
        }

        @Override
        public void addMetaHeader() {
            dobj.putString(0x00020010, VR.UI, "1.2.840.10008.1.2.1");
        }

        @Override
        public Iterator<DicomElementI> iterator() {
            return new MizerDicomObject.DOIterator();
        }

        /**
         * resolve the tagpath into integer tag array in the context of this DICOM object.
         * @param tagPath the tagpath to resolve.
         * @return optional array of tags. Not present if tagpath is not present in the object.
         */
        @Override
        public Optional<int[]> resolveTagPath(TagPath tagPath) {
            List<Integer> tagArray = new ArrayList<>();
            DicomObject item = dobj;
            boolean create = false;

            for (Tag t : tagPath.getTags()) {
                if (t instanceof TagSequence) {
                    TagSequence ts = (TagSequence) t;
                    int itemNumber = ts.getItemNumberAsInt();
                    Tag tag = ts.getTag();
                    int theTag = t.asInt();

                    if (tag instanceof TagPrivate) {
                        TagPrivate tagPrivate = (TagPrivate) tag;
                        theTag = item.resolveTag(theTag, tagPrivate.getPvtCreatorID(), create);
                        if (theTag == -1) {
                            return Optional.ofNullable(null);
                        }
                    }
                    DicomElement de = item.get(theTag);
                    if (de == null) {
                        return Optional.ofNullable(null);
                    }
                    tagArray.add(theTag);
                    if( hasItem(de, itemNumber)) {
                        tagArray.add(itemNumber);
                        item = de.getDicomObject(itemNumber);
                    } else {
                        return Optional.ofNullable(null);
                    }
                } else if (t instanceof TagPublic) {
                    if (! item.contains(t.asInt())) {
                        return Optional.ofNullable(null);
                    }
                    tagArray.add(t.asInt());
                } else if (t instanceof TagPrivateCreator) {
                    TagPrivateCreator tpc = (TagPrivateCreator) t;
                    int resolvedTag = item.resolveTag(t.asInt(), tpc.getPvtCreatorID(), create);
                    if (resolvedTag == -1) {
                        return Optional.ofNullable(null);
                    }
                    tagArray.add(resolvedTag);
                } else if (t instanceof TagPrivate) {
                    TagPrivate tagPrivate = (TagPrivate) t;
                    int resolvedTag = item.resolveTag(t.asInt(), tagPrivate.getPvtCreatorID(), create);
                    if (resolvedTag == -1) {
                        return Optional.ofNullable(null);
                    }
                    if (! item.contains(resolvedTag)) {
                        return Optional.ofNullable(null);
                    }
                    tagArray.add(resolvedTag);
                } else {
                    logger.debug("Error resolving tag of unimplemented type: {}", t);
                }
            }
            return Optional.of(TagPath.copyListToArray(tagArray));
        }

        private boolean hasItem(DicomElement de, int itemNumber) {
            return itemNumber < de.countItems();
        }

        public int[] resolve(TagPath tagPath, boolean create) {
            List<Integer> tagArray = new ArrayList<>();
            DicomObject tmpdobj = dobj;

            for (Tag t : tagPath.getTags()) {
                if (t instanceof TagSequence) {
                    TagSequence ts = (TagSequence) t;
                    Tag tag = ts.getTag();
                    if (tag instanceof TagPrivate) {
                        TagPrivate tagPrivate = (TagPrivate) tag;
                        int resolvedTag = tmpdobj.resolveTag(tag.asInt(), tagPrivate.getPvtCreatorID(), create);
                        if (resolvedTag == -1) {
                            DicomElement de = tmpdobj.putSequence(tag.asInt());
                            tmpdobj.putString(tagPrivate.getPvtCreatorIDTag(), VR.LO, tagPrivate.getPvtCreatorID());
                            tmpdobj = de.getDicomObject();
                            resolvedTag = tagPrivate.getPvtCreatorIDTag();
                        }
                        tagArray.add(resolvedTag);
                        tagArray.add(ts.getItemNumberAsInt());
                    } else if (tag instanceof TagPublic) {
                        DicomElement de = tmpdobj.get(tag.asInt());
                        if (de == null) {
                            de = tmpdobj.putSequence(tag.asInt());
                            de.addDicomObject(new BasicDicomObject());
                        }
                        tmpdobj = de.getDicomObject();
                        tagArray.add(tag.asInt());
                        tagArray.add(ts.getItemNumberAsInt());
                    }
                } else if (t instanceof TagPublic) {
                    tagArray.add(t.asInt());
                } else if (t instanceof TagPrivateCreator) {
                    TagPrivateCreator tpc = (TagPrivateCreator) t;
                    int resolvedTag = tmpdobj.resolveTag(t.asInt(), tpc.getPvtCreatorID(), false);
                    tagArray.add(resolvedTag);
                } else if (t instanceof TagPrivate) {
                    TagPrivate tagPrivate = (TagPrivate) t;
                    int resolvedTag = tmpdobj.resolveTag(t.asInt(), tagPrivate.getPvtCreatorID(), true);
                    if (resolvedTag == -1) {
                        tmpdobj.putString(tagPrivate.getPvtCreatorIDTag(), VR.LO, tagPrivate.getPvtCreatorID());
                        resolvedTag = tagPrivate.getPvtCreatorIDTag();
                    }
                    tagArray.add(resolvedTag);
                } else {
                    logger.debug("Error resolving tag of unimplemented type: {}", t);
                }
            }
            return TagPath.copyListToArray(tagArray);
        }

        @Override
        public DicomObjectI getDicomObject(Tag tag) {
            return null;
        }

        @Override
        public Optional<DicomObjectI> getItem(TagPath tagPath) {
            if( tagPath.isSingular() && tagPath.isSequence()) {
                return Optional.of( new MizerDicomObject( dobj.getNestedDicomObject( tagPath.getTagsAsArray())));
            }
            return Optional.empty();
        }

        /**
         * getItem
         * @param tags tag array of item
         * @return the item or null if not found.
         */
        @Override
        public DicomObjectI getItem( int[] tags) {
            try {
                return new MizerDicomObject(dobj.getNestedDicomObject(tags));
            } catch (Exception e) {
                logger.error("Error finding item {}", tags);
                return null;
            }
        }

        @Override
        public DicomElementI get(int tag) {
            return getElement( tag);
        }

        @Override
        public DicomElementI get(Tag tag) {
            return getElement( tag.asInt());
        }

        @Override
        public DicomElementI getElement( int tag) {
            return new MizerDicomElement( dobj.get( tag));
        }

        /**
         * Add the provided string value to the private tag. Use this if you want to let dcm4che manage the assignment
         * and resolution of private blocks.
         *
         * For a tag of form 0xggggbbee, putString ignores bb and "does the right thing", creating the private creator
         * ID element if needed and then puts the tag in the right block.
         *
         * Convenient for creating test DICOM objects.
         *
         * @param tag          The tag to write to.
         * @param pvtCreatorID The ID of the private tag creator.
         * @param value        The value to write to the tag.
         *
         * @return The ID of the resolved tag.
         */
        @Override
        public int putCreatorIDString(int tag, String pvtCreatorID, String value) {
            int t = dobj.resolveTag(tag, pvtCreatorID, true);
            VR vr = vr(t);
            putString(t,vr,value);
            return t;
        }

        /**
         * putString to int[] tags for all VRs.
         * dcm4che2 does not have a putString for all VRs. Revert to putBytes where needed.
         *
         * @param tags the attribute's tag array
         * @param vr the dcm4che2 VR
         * @param s the attribute's value.
         */
        private void putString(int[] tags, VR vr, String s) {
            // VR is not an enum.
            switch (vr.toString()) {
                case "OB":
                case "OD":
                case "OF":
                case "OW":
                case "UN":
                    // dcm4che2 does not have OL, OV
//                case "OL":
//                case "OV":
                    dobj.putBytes(tags, vr, s.getBytes(StandardCharsets.UTF_8));
                    break;
                default:
                    dobj.putString(tags, vr, s);
                    break;
            }
        }

        /**
         * putString to int tag for all VRs.
         * dcm4che2 does not have a putString for all VRs. Revert to putBytes where needed.
         *
         * @param tag the tag to be written to
         * @param vr the VR encoding to use
         * @param s the string to be written.
         */
        private void putString(int tag, VR vr, String s) {
            // VR is not an enum.
            switch (vr.toString()) {
                case "OB":
                case "OD":
                case "OF":
                case "OW":
                case "UN":
                    // dcm4che2 does not have OL, OV
//                case "OL":
//                case "OV":
                    dobj.putBytes(tag, vr, s.getBytes(StandardCharsets.UTF_8));
                    break;
                default:
                    dobj.putString(tag, vr, s);
                    break;
            }
        }

        @Override
        public void putString(int[] tags, String s) {
            putString( tags, vr(tags), s);
        }

        @Override
        public void putString(int[] tags, String vrString, String s) {
            VR vr = stringToVR(vrString);
            if (VR.UN.equals(vr)) {
                vr = vr(tags);
            }
            putString( tags, vr, s);
        }

        @Override
        public void putStrings(int[] tags, String[] s) {
            putString(tags, joinMultipleValues(s));
        }

        @Override
        public void putStrings(int[] tags, String vr, String[] s) {
            putString(tags, vr, joinMultipleValues(s));
        }

        @Override
        public void putString(int tag, String s) {
            putString(tag, vr(tag), s);
        }

        @Override
        public void putString(int tag, String vr, String s) {
            putString(tag, vr(tag), s);
        }

        @Override
        public void putStrings(int tag, String[] s) {
            putString(tag, vr(tag), joinMultipleValues(s));
        }

        @Override
        public void putStrings(int tag, String vr, String[] s) {
            putString(tag, vr(tag), joinMultipleValues(s));
        }

        public void removeTag(int tag) {
            dobj.remove(tag);
        }

        public void removePrivateTag(int tag, String pvtCreatorID) {
            dobj.remove(dobj.resolveTag(tag, pvtCreatorID));
        }

        /**
         * Write the object to the output Stream.
         *
         * Insure group 2 attributes match object's.
         *
         * @param os
         * @throws MizerException
         */
        @Override
        public void write(OutputStream os) throws MizerException {
            try (final DicomOutputStream out = new DicomOutputStream(os)) {
                String tsString = dobj.getString( 0x00020010);
                if( tsString == null) {
                    dobj.putString( 0x00020010, VR.UI, "1.2.840.10008.1.2.1"); // ExplicitVRLittleEndian
                }
                String sopClassUID = dobj.getString( 0x00080016);
                dobj.putString( 0x00020002, VR.UI, sopClassUID);
                String sopInstanceUID = dobj.getString( 0x00080018);
                dobj.putString( 0x00020003, VR.UI, sopInstanceUID);
                out.writeDicomFile(dobj);
//                if( logger.isTraceEnabled()) {
//                    logger.trace("Writing object to stream: {}", this.toCompleteString());
//                }
            } catch (IOException e) {
                throw new MizerException(e);
            }
        }

        @Override
        public void read( InputStream is) throws MizerException {
            try (final DicomInputStream dis = new DicomInputStream(is)) {
                dobj = dis.readDicomObject();
            } catch (IOException e) {
                throw new MizerException(e);
            }
        }

        @Override
        public void read( File f) throws MizerException {
            try (final InputStream fin = getInputStream(f); final DicomInputStream dis = new DicomInputStream(fin)) {
                dobj = dis.readDicomObject();
            } catch (IOException e) {
                throw new MizerException(e);
            }
        }

        @Override
        public boolean contains(Tag tag) {
            if (tag instanceof TagPublic) {
                return contains((TagPublic) tag);
            } else if (tag instanceof TagPrivate) {
                return contains((TagPrivate) tag);
            } else if (tag instanceof TagSequence) {
                return contains((TagSequence) tag);
            }
            return false;
        }

        @Override
        public boolean contains(TagPath tagPath) {
            return contains( tagPath.getTagsAsArray());
        }

        @Override
        public boolean contains(int tag) {
            return dobj.contains(tag);
        }

        @Override
        public boolean contains(int tag, String pvtCreatorID) {
            return ! dobj.contains( dobj.resolveTag( tag, pvtCreatorID));
        }

        /**
         * return true if an element exists at the tagpath.
         *
         * Handy for unit tests.
         *
         * @param tagArray The tag path to evaluate.
         *
         * @return Whether the element exists at the submitted tag path.
         */
        @Override
        public boolean contains(int[] tagArray) {
            if (isEven(tagArray.length)) {
                return dobj.getNestedDicomObject(tagArray) != null;
            } else {
                return dobj.get(tagArray) != null;
            }
        }

        private boolean isEven(int i) {
            return (i | 1) > i;
        }

        @Override
        public String getPrivateCreator(int t) {
            return dobj.getPrivateCreator(t);
        }

        @Override
        public boolean isSequenceElement(int t) {
            return dobj.get(t).hasItems();
        }

        @Override
        public boolean isEmpty() {
            return dobj.isEmpty();
        }

        @Override
        public boolean isEmpty(int tag) {
            return dobj.get(tag).isEmpty();
        }
        @Override
        public boolean isEmpty(int[] tags) {
            return dobj.get(tags).isEmpty();
        }

        @Override
        public int size() {
            return dobj.size();
        }

        @Override
        public void deleteAllPrivateTags() {
            deleteAllPrivateTags(dobj);
        }

        @Override
        public String toString() {
            return dobj.toString();
        }

        @Override
        public String toCompleteString() {
            StringBuffer sb = new StringBuffer();
            DicomObjectToStringParam dp = DicomObjectToStringParam.getDefaultParam();
            DicomObjectToStringParam params = new DicomObjectToStringParam(dp.name, dp.valueLength, dp.numItems, 128, Integer.MAX_VALUE, dp.indent, dp.lineSeparator);
            dobj.toStringBuffer(sb, params);
            return sb.toString();
        }

        @Override
        public void dump(PrintStream ps) {
            DumpDicomTagVisitor visitor = new DumpDicomTagVisitor( this, ps);
            visitor.visit( this);
        }

        private InputStream getInputStream(final File f) throws IOException {
            final InputStream fin = new BufferedInputStream( new FileInputStream(f));
            return f.getName().endsWith("gz") ? new GZIPInputStream(fin) : fin;
        }

        private void deleteEmptyItems(org.dcm4che2.data.DicomObject dicomObject) {
            Iterator<DicomElement> it = dicomObject.iterator();
            while (it.hasNext()) {
                DicomElement de = it.next();
                deleteEmptyItems(de, dicomObject);
            }
        }

        private void deleteEmptyItems(DicomElement de, DicomObject parent) {
            if (de.hasItems()) {
                if( ! de.isEmpty()) {
                    if( 0x7FE00010 != de.tag()) {
                        List<DicomObject> items = getItems(de);

                        for (DicomObject item : items) {
                            if (item.isEmpty()) {
                                de.removeDicomObject(item);
                            } else {
                                deleteEmptyItems(item);
                            }
                        }
                        if (ifItemsAreEmpty(items)) {
                            parent.remove(de.tag());
                        }
                    }
                }
                else {
                    parent.remove( de.tag());
                }
            }
        }

        private boolean ifItemsAreEmpty( List<DicomObject> items) {
            for( DicomObject item: items) {
                if( ! item.isEmpty()) return false;
            }
            return true;
        }

        private List<DicomObject> getItems( DicomElement de) {
            List<DicomObject> items = new ArrayList<>();
            if( de.hasItems() && ( ! de.isEmpty())) {
                for (int i = 0; i < de.countItems(); i++) {
                    items.add(de.getDicomObject(i));
                }
            }
            return items;
        }

        /**
         * listPrivateBlock
         * Return a list of all tags in the specified private block.
         * Does not descend into sequences.
         *
         * @param dicomObject object under scrutiny.
         * @param tag tag specifying private block. Can be any tag in the private block including private-creator-id tag. Returned list is empty if tag is not Private.
         * @return list of private tags in block, excluding private-creator-id tag. List is empty if there are no private tags in block or if specified tag is not a private tag.
         */
        private List<Integer> listPrivateBlock( DicomObject dicomObject, int tag) {
            List<Integer> privateTags = new ArrayList<>();
            int privateCreatorID;
            try {
                privateCreatorID = Tag.getPrivateCreatorIDTag(tag);
            }
            catch (IllegalArgumentException e) {
                String msg = "Tag is not private: " + Integer.toHexString( tag);
                logger.warn( msg, e);
                return privateTags;
            }
            int group = Tag.getGroup( privateCreatorID);
            int block = Tag.getPrivateCreatorBlock( privateCreatorID);

            for( Iterator<DicomElement> it = dicomObject.datasetIterator(); it.hasNext(); ) {
                DicomElement de = it.next();
                if( Tag.isPrivateDataTag( de.tag()) && ! Tag.isPrivateCreatorDataTag( de.tag())) {
                    if( group == Tag.getGroup( de.tag()) && block == Tag.getPrivateBlock( de.tag())) {
                        privateTags.add(de.tag());
                    }
                }
            }
            return privateTags;
        }

        public boolean isEmptyPrivateBlock( int tag) {
            return listPrivateBlock( this.dobj, tag).isEmpty();
        }

        private void deleteAllPrivateTags(org.dcm4che2.data.DicomObject dicomObject) {

            for (Iterator<DicomElement> it = dicomObject.datasetIterator(); it.hasNext(); ) {
                DicomElement de = it.next();
                if (isPrivateDataElement(de.tag())) {
                    dicomObject.remove(de.tag());
                } else if (de.hasItems() && ( de.tag() != 0x7FE00010)) {
                    for (int i = 0; i < de.countItems(); i++) {
                        deleteAllPrivateTags(de.getDicomObject(i));
                    }
                }

            }
        }

        private boolean contains(TagPublic tagPublic) {
            return dobj.contains(tagPublic.asInt());
        }

        private boolean contains(TagPrivate tagPrivate) {
            int tagInt = dobj.resolveTag(tagPrivate.asInt(), tagPrivate.getPvtCreatorID(), false);
            return dobj.contains(tagInt);
        }

        private boolean contains(TagSequence tagSequence) {
            boolean b = false;
            DicomElement dicomElement = dobj.get(tagSequence.getTag().asInt());
            if (dicomElement != null) {
                b = (dicomElement.getDicomObject(tagSequence.getItemNumberAsInt()) != null);
            }
            return b;
        }

        private class DOIterator implements Iterator<DicomElementI> {

            Iterator<DicomElement> iterator = dobj.iterator();

            @Override
            public boolean hasNext() {
                return iterator.hasNext();
            }

            @Override
            public DicomElementI next() {
                return new MizerDicomElement(iterator.next());
            }

            @Override
            public void remove() {
                throw new UnsupportedOperationException();
            }
        }

        private VR stringToVR(String vrString) {
            switch(vrString) {
                case "UN_SIEMENS":
                    return VR.UN_SIEMENS;
                case "AS":
                    return VR.AS;
                case "AT":
                    return VR.AT;
                case "CS":
                    return VR.CS;
                case "DA":
                    return VR.DA;
                case "DS":
                    return VR.DS;
                case "DT":
                    return VR.DT;
                case "FD":
                    return VR.FD;
                case "FL":
                    return VR.FL;
                case "IS":
                    return VR.IS;
                case "LO":
                    return VR.LO;
                case "LT":
                    return VR.LT;
                case "OB":
                    return VR.OB;
                case "OF":
                    return VR.OF;
                case "OW":
                    return VR.OW;
                case "PN":
                    return VR.PN;
                case "SH":
                    return VR.SH;
                case "SL":
                    return VR.SL;
                case "SQ":
                    return VR.SQ;
                case "SS":
                    return VR.SS;
                case "ST":
                    return VR.ST;
                case "TM":
                    return VR.TM;
                case "UI":
                    return VR.UI;
                case "UL":
                    return VR.UL;
                case "UN":
                    return VR.UN;
                case "US":
                    return VR.US;
                case "UT":
                    return VR.UT;
            }
            return VR.UN;
        }
    }

    private static class MizerDicomElement implements DicomElementI {

        private final DicomElement element;

        public MizerDicomElement(final DicomElement element) {
            this.element = element;
        }

        @Override
        public int tag() {
            return element.tag();
        }

        public String tagString() {
            return Integer.toHexString( element.tag());
        }

        @Override
        public boolean hasItems() {
            return element.hasItems();
        }

        @Override
        public int countItems() {
            return element.countItems();
        }

        @Override
        public DicomObjectI getDicomObject(int i) {
            return new MizerDicomObject( element.getDicomObject( i));
        }

        @Override
        public void removeItem( int i) {
            element.removeDicomObject(i);
        }

        @Override
        public boolean isUID() {
            return element.vr() == VR.UI;
        }

        @Override
        public String getVRAsString() {
            return element.vr().toString();
        }

    }

    private static final Logger logger = LoggerFactory.getLogger(DicomObjectFactory.class);
}
