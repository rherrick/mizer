/*
 * DicomEdit: DicomObjectI
 * XNAT http://www.xnat.org
 * Copyright (c) 2017, Washington University School of Medicine
 * All Rights Reserved
 *
 * Released under the Simplified BSD.
 */

package org.nrg.dicom.mizer.objects;

import org.dcm4che2.data.DicomObject;
import org.nrg.dicom.mizer.exceptions.MizerException;
import org.nrg.dicom.mizer.tags.Tag;
import org.nrg.dicom.mizer.tags.TagPath;
import org.nrg.dicom.mizer.values.Value;

import java.io.*;
import java.time.Period;
import java.util.Iterator;
import java.util.Optional;

/**
 * <p> Interface for manipulating DICOM Objects. </p>
 *
 * <p> These are the methods DicomEdit uses to manipulate DICOM Objects. It provides a facade behind which specific
 * versions of Dicom libraries can be hidden, e.g. dcm4che2. This separation facilitates the migration to other libs. </p>
 *
 * <p> {@link TagPath} is the primary object used to address tags within DICOM objects. {@link TagPath} can
 * contain wild-card characters and/or private-creator IDs so TagPaths don't necessarily address unique elements. It is up to
 * the DicomObject to resolve these references in the context of the a DicomObject's instance. </p>
 *
 * <p> Some methods are overloaded with int or int[] tag parameters and are primarily provided for convenience in initializing
 * and validating unit tests. </p>
 *
 * <p> DicomEdit manipulates all tag values as Strings regardless of the tag's value representation (VR). </p>
 *
 */
public interface DicomObjectI {

    String getString(TagPath tagPath);

    String getString(int tag);

    String getString(int... tag);

    String[] getStrings(TagPath tagPath);

    String[] getStrings(int tag);

    String[] getStrings(int... tag);

    byte[] getBytes( int tag) throws IOException;

    void putBytes( int tag, String vr, byte[] b);

    String getVR( int tag);
    String getVR( TagPath tp);

    Optional<Period> getAge(int tag);

    int resolvePrivateTag(int tag, String pvtCreator, boolean create);

    Optional<int[]> resolveTagPath(TagPath tagPath);

    void assign(TagPath tp, String assignedString);

    void assignIfExists(TagPath tp, Value assignedString);

    void assign( int tag, Value value);

    void assign(int[] tags, Value assignedValue);

    void delete( TagPath tagPath);

    void delete(int tag);

    void delete(int[] tags);

    void deleteAllTags();

    void read( InputStream is) throws MizerException;

    void read( File file) throws MizerException;

    void write(OutputStream os) throws MizerException;

    boolean contains( int tag);
    boolean contains( int[] tags);
    boolean contains( int tag, String pvtCreatorID);

    String getPrivateCreator( int tag);

    boolean isSequenceElement(int t);

    void addMetaHeader();

    Iterator<DicomElementI> iterator();

    boolean isEmpty();
    boolean isEmpty(int tag);
    boolean isEmpty(int[] tags);
    int size();

    boolean contains( Tag tag);

    boolean contains( TagPath tagPath);

    DicomObjectI getDicomObject(Tag tag);

    Optional<DicomObjectI> getItem(TagPath tagPath);

    DicomObjectI getItem( int[] tags);

    DicomElementI get( int tag);
    DicomElementI get( Tag tag);

    DicomElementI getElement( int tag);

    void putString(int[] tags, String s);
    void putString(int[] tags, String vr, String s);
    void putStrings(int[] tags, String[] s);
    void putStrings(int[] tags, String vr, String[] s);

    void putString(int tag, String s);
    void putString(int tag, String vr, String s);
    void putStrings(int tag, String[] s);
    void putStrings(int tag, String vr, String[] s);

    int putCreatorIDString(int tag, String pvtCreatorID, String value);

    void removeTag(int tag);

    void removePrivateTag(int tag, String pvtCreatorID);

    void deleteAllPrivateTags();

    void deleteEmptyPrivateBlocks();

    boolean isEmptyPrivateBlock( int tag);

    String toCompleteString();

    void dump(PrintStream ps);

    /**
     * poke a hole for DicomEdit v4.
     *
     * @return Dcm4che2 instance of a Dicom Object.
     */
    DicomObject getDcm4che2Object();
}
