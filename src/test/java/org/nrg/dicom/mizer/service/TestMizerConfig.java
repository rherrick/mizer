/*
 * dicom-edit4: org.nrg.dcm.mizer.TestMizerConfig
 * XNAT http://www.xnat.org
 * Copyright (c) 2017, Washington University School of Medicine
 * All Rights Reserved
 *
 * Released under the Simplified BSD.
 */

package org.nrg.dicom.mizer.service;

//import org.nrg.dicom.dicomedit.DE6ScriptFactory;
//import org.nrg.dicom.dicomedit.mizer.DE6Mizer;
//import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan({ "org.nrg.dicom.mizer.service.impl"})
public class TestMizerConfig {
//    @Bean
//    public Mizer de6Mizer() {
//        return new DE6Mizer( new DE6ScriptFactory());
//    }

}
